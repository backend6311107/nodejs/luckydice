const express = require("express");
const app = express();
const mongoose = require("mongoose");
mongoose.set('strictQuery', true)
const port = 8000;

const userRouter = require("./app/routes/userRouter");
const diceHistoryRouter = require("./app/routes/diceHistoryRouter");
const prizeRouter = require("./app/routes/prizeRouter");
const voucherRouter = require("./app/routes/voucherRouter");
const prizeHistoryRotuer = require("./app/routes/prizeHistoryRouter");
const voucherHistoryRouter = require("./app/routes/voucherHistoryRouter");

app.use(express.static("app"));
app.use(express.json());

app.use((req, res, next) => {
    console.log(`Current time: ${new Date()}`);
    next();
});

app.use((req, res, next) => {
    console.log(`Request method: ${req.method}`);
    next();
});

app.use("/api", userRouter);
app.use("/api", diceHistoryRouter);
app.use("/api", prizeRouter);
app.use("/api", voucherRouter);
app.use("/api", prizeHistoryRotuer);
app.use("/api", voucherHistoryRouter);

async function startServer() {
    try {
        await mongoose.connect("mongodb://127.0.0.1:27017/ex_dice");
        console.log(`Connect MongoDB successfully !!!`);
        app.listen(port, () => {
            console.log(`App running on port: ${port}`);
        });
    } catch (error) {
        console.error(`Error connecting to MongoDB: ${error}`);
    }
}

startServer();

