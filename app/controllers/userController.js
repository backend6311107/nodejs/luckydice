const mongoose = require("mongoose");
const userModel = require("../../app/models/userModel");

const getAllUser = async (req, res) => {
    try {
        const dataUser = await userModel.find();
        return res.status(200).json({
            status: "Get all user successfully",
            data: dataUser
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
};

const getUserById = async (req, res) => {
    userID = req.params.userId;

    if (!mongoose.Types.ObjectId.isValid(userID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `User ID: ${userID} không hợp lệ`
        });
    }

    try {
        const dataUser = await userModel.findById(userID);
        if (dataUser === null) {
            return res.status(400).json({
                status: "Bad request",
                message: `User ID: ${userID} không tồn tại trong hệ thống`
            });
        }
        return res.status(200).json({
            status: `Get user by ID: ${userID} successfully`,
            data: dataUser
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        });
    }
};

const createUser = async (req, res) => {
    const body = req.body;

    if (!body.username || !isNaN(body.username)) {
        return res.status(400).json({
            status: "Bad request",
            message: "username không hợp lệ"
        });
    }
    if (!body.firstname || !isNaN(body.firstname)) {
        return res.status(400).json({
            status: "Bad request",
            message: "firstname Không hợp lệ"
        });
    }
    if (!body.lastname || !isNaN(body.lastname)) {
        return res.status(400).json({
            status: "Bad request",
            message: "lastname không hợp lệ"
        });
    }

    const newUser = {
        _id: new mongoose.Types.ObjectId(),
        username: body.username,
        firstname: body.firstname,
        lastname: body.lastname
    };

    try {
        const data = await userModel.create(newUser);
        return res.status(201).json({
            status: "Create new user successfully.",
            data: data
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        });
    }
};

const updateUserById = async (req, res) => {
    const userID = req.params.userId;
    const body = req.body;

    if (!mongoose.Types.ObjectId.isValid(userID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `User ID: ${userID} không hợp lệ`
        });
    }
    if (body.username !== undefined && (!body.username || !isNaN(body.username))) {
        return res.status(400).json({
            status: "Bad request",
            message: `username Không hợp lệ`
        });
    }
    if (body.firstname !== undefined && (!body.firstname || !isNaN(body.firstname))) {
        return res.status(400).json({
            status: "Bad request",
            message: `firstname không hợp lệ`
        });
    }
    if (body.lastname !== undefined && (!body.lastname || !isNaN(body.lastname))) {
        return res.status(400).json({
            status: "Bad request",
            message: `lastname không hợp lệ`
        });
    }

    const updateUser = {};
    updateUser.username = body.username;
    updateUser.firstname = body.firstname;
    updateUser.lastname = body.lastname;

    try {
        const dataUser = await userModel.findByIdAndUpdate(userID, updateUser);
        if (dataUser === null) {
            return res.status(400).json({
                status: "Bad request",
                message: `User ID: ${userID} không tồn tại trong hệ thống`
            });
        }
        return res.status(200).json({
            status: `Update user by ID: ${userID} successfully`,
            data: dataUser
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        });
    }
};

const deleteUserById = async (req, res) => {
    const userID = req.params.userId;

    if (!mongoose.Types.ObjectId.isValid(userID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `User ID: ${userID} không hợp lệ`
        });
    }

    try {
        const dataUser = await userModel.findByIdAndDelete(userID);
        if (dataUser === null) {
            return res.status(400).json({
                status: "Bad request",
                message: `User ID: ${userID} không tồn tại trong hệ thống`
            });
        }
        return res.status(200).json({
            status: `Delete User by ID: ${userID} successfully`
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        });
    }
};

module.exports = {
    getAllUser,
    getUserById,
    createUser,
    updateUserById,
    deleteUserById
};