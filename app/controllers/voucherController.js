const mongoose = require("mongoose");
const voucherModel = require("../models/voucherModel");

const getAllVoucher = async (req, res) => {
    try {
        const dataVoucher = await voucherModel.find();
        return res.status(200).json({
            status: "Get all voucher successfully",
            data: dataVoucher
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        });
    }
};

const getVoucherById = async (req, res) => {
    const voucherID = req.params.voucherId;

    if (!mongoose.Types.ObjectId.isValid(voucherID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `Voucher ID: ${voucherID} không hợp lệ`
        });
    }

    try {
        const dataVoucher = await voucherModel.findById(voucherID);
        if (!dataVoucher) {
            return res.status(400).json({
                status: "Bad request",
                message: `Voucher ID: ${voucherID} không tồn tại`
            });
        }
        return res.status(200).json({
            status: `Get voucher by ID: ${voucherID} successfully`,
            data: dataVoucher
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        });
    }
};

const createVoucher = async (req, res) => {
    const body = req.body;
    const dataCode = await voucherModel.findOne({ code: body.code });

    if (dataCode) {
        return res.status(400).json({
            status: "Bad request",
            message: `Code voucher ${body.code} này đã có trong hệ thống`
        });
    }
    if (!isNaN(body.code) || body.code.trim() === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "code không hợp lệ"
        });
    }
    if (isNaN(body.discount) || body.discount.trim() === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "discount Không hợp lệ"
        });
    }
    if (body.note) {
        if (!isNaN(body.note) || body.note.trim() === '') {
            return res.status(400).json({
                status: "Bad request",
                message: "note không hợp lệ"
            });
        }
    }

    const newVoucher = {
        _id: new mongoose.Types.ObjectId(),
        code: body.code,
        discount: body.discount,
        note: body.note
    }

    try {
        const dataVoucher = await voucherModel.create(newVoucher);
        return res.status(200).json({
            status: "Create new voucher successfully",
            data: dataVoucher
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        });
    }
};

const updateVoucherById = async (req, res) => {
    const voucherID = req.params.voucherId;
    const body = req.body;

    if (!mongoose.Types.ObjectId.isValid(voucherID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `Voucher ID: ${voucherID} không hợp lệ`
        });
    }
    if (body.code !== undefined && (!isNaN(body.code) || body.code.trim() === "")) {
        return res.status(400).json({
            status: "Bad request",
            message: "Code không hợp lệ"
        });
    }
    if (body.discount !== undefined && (isNaN(body.discount) || body.discount < 0)) {
        return res.status(400).json({
            status: "Bad request",
            message: "Discount không hợp lệ"
        });
    }
    if (body.note !== undefined && (!isNaN(body.note) || body.note.trim() === "")) {
        return res.status(400).json({
            status: "Bad request",
            message: "note không hợp lệ"
        });
    }

    const updateVoucher = {};
    updateVoucher.code = body.code;
    updateVoucher.discount = body.discount;
    updateVoucher.note = body.note;

    try {
        
        const dataVoucher = await voucherModel.findByIdAndUpdate(voucherID, updateVoucher);
        if (!dataVoucher) {
            return res.status(400).json({
                status: "Bad request",
                message: `Voucher ID: ${voucherID} không tồn tại trong hệ thống`
            });
        }
        return res.status(200).json({
            status: "Bad request",
            message: `Update voucher by ID: ${voucherID} successfully`
        });
    } catch (error) {
        return res.staus(500).json({
            status: "Internal server error",
            message: error.message
        });
    }
};

const deleteVoucherById = async (req, res) => {
    const voucherID = req.params.voucherId;
    if (!mongoose.Types.ObjectId.isValid(voucherID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `Voucher ID: ${voucherID} không hợp lệ`
        });
    }

    try {
        const dataVoucher = await voucherModel.findByIdAndDelete(voucherID);
        if (!dataVoucher) {
            return res.status(400).json({
                status: `Bad request`,
                message: `Voucher ID: ${voucherID} không tồn tại`
            });
        }
        return res.status(200).json({
            status: `Delete voucher by ID: ${voucherID} successfully`
        });
    } catch (error) {
        return res.status(200).json({
            status: "Internal server error",
            message: error.message
        });
    }
};

module.exports = {
    getAllVoucher,
    getVoucherById,
    createVoucher,
    updateVoucherById,
    deleteVoucherById
}