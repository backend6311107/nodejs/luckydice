const mongoose = require("mongoose");
const prizeModel = require("../models/prizeModel");

const getAllPrize = async (req, res) => {
    try {
        const dataPrize = await prizeModel.find();
        return res.status(200).json({
            status: "Get all prize successfully",
            data: dataPrize
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        });
    }
};

const getPrizeById = async (req, res) => {
    const prizeID = req.params.prizeId;

    if (!prizeID || (typeof prizeID !== 'string') || (prizeID.length !== 24)) {
        return res.status(400).json({
            status: "Bad request",
            message: `Prize ID: ${prizeID} không hợp lệ`
        });
    }

    try {
        const dataPrize = await prizeModel.findById(prizeID);
        if (!dataPrize) {
            return res.status(400).json({
                status: "Bad request",
                message: `Prize ID: ${prizeID} không tồn tại trong hệ thống`
            });
        }
        return res.status(200).json({
            status: `Get prize by ID: ${prizeID} successfully`,
            data: dataPrize
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        });
    }
};

const createPrize = async (req, res) => {
    const body = req.body;

    if (!body.name || !isNaN(body.name)) {
        return res.status(400).json({
            status: "Bad request",
            message: "name không hợp lệ"
        });
    }
    if (body.description !== undefined && body.description.trim() === "") {
        return res.status(400).json({
            status: "Bad request",
            message: "description Không hợp lệ"
        });
    }

    const newPrize = {
        _id: new mongoose.Types.ObjectId(),
        name: body.name,
        description: body.description
    };

    try {
        const dataPrize = await prizeModel.create(newPrize);
        return res.status(200).json({
            status: "Create new prize successfully",
            data: dataPrize
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        });
    }
};

const updatePrizeById = async (req, res) => {
    const prizeID = req.params.prizeId.trim();
    const body = req.body;
    if (!prizeID || (typeof prizeID !== 'string') || (prizeID.length !== 24)) {
        return res.status(400).json({
            status: "Bad request",
            message: `Prize ID: ${prizeID} không hợp lệ`
        });
    }
    if (body.name !== undefined && (!body.name || !isNaN(body.name))) {
        return res.status(400).json({
            status: "Bad request",
            message: "Name không hợp lệ"
        });
    }

    if (body.description !== undefined && (!body.description || !isNaN(body.description))) {
        return res.status(400).json({
            status: "Bad request",
            message: "Description không hợp lệ"
        });
    }


    const updatePrize = {};
    updatePrize.name = body.name;
    updatePrize.description = body.description;

    try {
        const dataDice = await prizeModel.findByIdAndUpdate(prizeID, updatePrize);
        if (!dataDice) {
            return res.status(400).json({
                status: "Bad request",
                message: `Prize ID: ${prizeID} không tồn tại trong hệ thống`
            })
        }
        return res.status(200).json({
            status: `Update prize by ID: ${prizeID} successfully`,
            data: dataDice
        })
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        });
    }
};

const deletePrizeById = async (req, res) => {
    const prizeID = req.params.prizeId;

    if (!mongoose.Types.ObjectId.isValid(prizeID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `Prize ID: ${prizeID} không hợp lệ`
        });
    }

    try {
        const dataPrice = await prizeModel.findByIdAndDelete(prizeID);
        if(!dataPrice){
            return res.status(400).json({
                status: "Bad request",
                message: `Prize ID: ${prizeID} không tồn tại trong hệ thống`
            })
        }
        return res.status(200).json({
            status: `Delete prize by ID: ${prizeID} successfully`
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        });
    }
};

module.exports = {
    getAllPrize,
    getPrizeById,
    createPrize,
    updatePrizeById,
    deletePrizeById
}