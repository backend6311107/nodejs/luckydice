const mongoose = require("mongoose");
const { findByIdAndUpdate } = require("../models/diceHistoryModel");
const diceHistoryModel = require("../models/diceHistoryModel");
const userModel = require("../models/userModel")

const getAllDiceHistory = async (req, res) => {
    try {
        let condition = {};
        if(req.query.user){
            condition.user = req.query.user;
        }
        const dataDice = await diceHistoryModel.find(condition).populate({
            path: "user",
            select: "username"
        }).exec();
        return res.status(200).json({
            status: "Get all dice history successfully.",
            data: dataDice
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        });
    }
};

const getDiceHistoryById = async (req, res) => {
    const diceHistoryID = req.params.diceHistoryId;

    if (!mongoose.Types.ObjectId.isValid(diceHistoryID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `Dice history ID: ${diceHistoryID} không hợp lệ`
        });
    }

    try {
        const dataDice = await diceHistoryModel.findById(diceHistoryID).populate({
            path: "user",
            select: "username"
        }).exec();
        if (dataDice === null) {
            return res.status(400).json({
                status: `Bad request`,
                message: `Dice history ID: ${diceHistoryID} không tồn tại trong hệ thống`
            });
        }
        return res.status(200).json({
            status: `Get dice history by ID: ${diceHistoryID} successfully`,
            data: dataDice
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
};

const createDiceHistory = async (req, res) => {
    const body = req.body;

    if (!mongoose.Types.ObjectId.isValid(body.user)) {
        return res.status(400).json({
            status: "Bad request",
            message: `User ID: ${body.user} không hợp lệ`
        })
    }
    if (body.user) {
        const userID = await userModel.findOne({ _id: new mongoose.Types.ObjectId(body.user) });
        if (!userID) {
            return res.status(400).json({
                status: "Bad request",
                message: `User: ${userID} không tồn tại trong hệ thống`
            });
        }
        //Vì user là duy nhất, nên phải check xem user truyền vô đã tồn tại trong model dicehistory chưa
        // const userHistory = await diceHistoryModel.findOne({ user: new mongoose.Types.ObjectId(body.user) });
        // if (userHistory) {
        //     return res.status(400).json({
        //         status: "Bad request",
        //         message: "User này đã có dice history rồi."
        //     });
        // }
    }

    //let isUnique = false;
    //let newDice = 0;
    //while (!isUnique) {

     newDice = Math.floor(Math.random() * 6) + 1;
    //newDice = Math.floor(Math.random() * 10);
    //const diceHistory = await diceHistoryModel.findOne({ dice: newDice });
    // if (!diceHistory) {
    //     isUnique = true;
    // }
    //}

    const newDiceHistory = {
        _id: new mongoose.Types.ObjectId(),
        user: body.user,
        dice: newDice
    };

    console.log(newDiceHistory)
    try {
        const dataDiceHistory = await diceHistoryModel.create(newDiceHistory);
        return res.status(201).json({
            status: "Create new dice history successfully.",
            data: dataDiceHistory
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        });
    }
};

const updateDiceHistoryById = async (req, res) => {
    const diceHistoryID = req.params.diceHistoryId;
    const body = req.body;
    if (!mongoose.Types.ObjectId(diceHistoryID)) {
        return res.status(400).json({
            status: 'Bad request',
            message: `Dice history ID: ${diceHistoryID} không hợp lệ`,
        });
    }

    if (!diceHistoryID || (typeof diceHistoryID !== 'string') || (diceHistoryID.length !== 24)) {
        return res.status(400).json({
            status: 'Bad request',
            message: `Dice history ID: ${diceHistoryID} không hợp lệ`,
        });
    }
    const diceID = await diceHistoryModel.findOne({ _id: new mongoose.Types.ObjectId(diceHistoryID) });
    if (!diceID) {
        return res.status(400).json({
            status: 'Bad request',
            message: `Dice history ID: ${diceHistoryID} không tồn tại`,
        });
    }

    if (body.user !== undefined) {
        if (!mongoose.Types.ObjectId.isValid(body.user)) {
            return res.status(400).json({
                status: "Bad request",
                message: `User: ${body.user} không hợp lệ`
            });
        }

        const userID = await userModel.findOne({ _id: new mongoose.Types.ObjectId(body.user) });
        if (!userID) {
            return res.status(400).json({
                status: "Bad request",
                message: `User: ${body.user} không tồn tại trong hệ thống`
            });
        }

        const userDice = await diceHistoryModel.findOne({ user: new mongoose.Types.ObjectId(body.user) });
        if (userDice) {
            return res.status(400).json({
                status: "Bad request",
                message: `Dice history đã tồn tại user: ${body.user} này rồi`
            });
        }
    }

    if (body.dice !== undefined) {
        if (isNaN(body.dice) || body.dice.toString().trim() === "" || body.dice < 1 || body.dice > 6) {
            return res.status(400).json({
                status: "Bad request",
                message: "Dice không hợp lệ"
            });
        }
    }

    const updateDice = {};
    updateDice.user = body.user;
    updateDice.dice = body.dice;

    try {
        const dataDice = await diceHistoryModel.findByIdAndUpdate(diceHistoryID, updateDice);
        return res.status(201).json({
            status: `Update dice history by ID ${diceHistoryID} successfully`,
            data: dataDice
        });
    } catch (error) {
        return res.status(500).json({
            status: `Internal server error`,
            message: error.message
        });
    }
};

const deleteDiceHistoryById = async (req, res) => {
    const diceHistoryID = req.params.diceHistoryId;

    if (!diceHistoryID || (typeof diceHistoryID !== 'string') || (diceHistoryID.length !== 24)) {
        return res.status(400).json({
            status: 'Bad request',
            message: `Dice history ID: ${diceHistoryID} không hợp lệ`,
        });
    }

    try {
        const dataDice = await diceHistoryModel.findByIdAndDelete(diceHistoryID);
        if (dataDice === null) {
            return res.status(400).json({
                status: "Bad request",
                message: `Dice history ID: ${diceHistoryID} không tồn tại trong hệ thống`
            });
        }
        return res.status(200).json({
            status: `Delete dice history by ID: ${diceHistoryID} successfully`
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        });
    }
};

module.exports = {
    getAllDiceHistory,
    getDiceHistoryById,
    createDiceHistory,
    updateDiceHistoryById,
    deleteDiceHistoryById
}
