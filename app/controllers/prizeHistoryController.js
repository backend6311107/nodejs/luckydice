const mongoose = require("mongoose");
const prizeHistoryModel = require("../models/prizeHistoryModel");
const userModel = require("../models/userModel");
const prizeModel = require("../models/prizeModel");

const getAllPrizeHistory = async (req, res) => {
    try {
        const currentPage = parseInt(req.query.page) || 1; //Trang hiện tại
        const perPage = parseInt(req.query.limit) || 2; //Số lượng mục trên mỗi trang
        const skip = (currentPage - 1) * perPage; //Chỉ số mục bắt đầu
        const limit = perPage; //Số lượng mục lấy
        const dataPrize = await prizeHistoryModel.find().skip(skip).limit(limit).populate({
            path: "user",
            //Thêm -_id để bỏ trương _id của model user, chỉ lấy username của model user
            select: "username -_id"
        }).populate({
            path: "prize",
            select: "name"
        }).exec();
        //Nếu muốn in ra trường user không phải là {object} mà là chuỗi thì:
        // const data = dataPrize.map(item => {
        //     return {
        //         ...item.toObject(),
        //         user: item.user.username
        //     };
        // });
        return res.status(200).json({
            status: "Get all prize history successfully",
            data: dataPrize
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        });
    }
};

const getPrizeHistoryById = async (req, res) => {
    const prizeHisID = req.params.prizeHistoryId;

    if (!mongoose.Types.ObjectId.isValid(prizeHisID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `Prize history ID: ${prizeHisID} không hợp lệ`
        });
    }

    try {
        const dataPrize = await prizeHistoryModel.findById(prizeHisID).populate({
            path: "user",
            select: "username -_id"
        }).populate({
            path: "prize",
            select: "name "
        }).exec();
        if (!dataPrize) {
            return res.status(200).json({
                status: `Prize history ID: ${prizeHisID} không tồn tại trong hệ thống`
            });
        }
        return res.status(200).json({
            status: `Get prize history by ID: ${prizeHisID} successfully`,
            data: dataPrize
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        });
    }
};

const createPrizeHistory = async (req, res) => {
    const body = req.body;
    if (!mongoose.Types.ObjectId.isValid(body.user)) {
        return res.status(400).json({
            status: "Bad request",
            message: `User ID: ${body.user} không hợp lệ`
        });
    }
    const userID = await userModel.findById(body.user);
    if (!userID) {
        return res.status(400).json({
            status: "Bad request",
            message: `User ID: ${body.user} không tồn tại.`
        });
    }
    if (!mongoose.Types.ObjectId.isValid(body.prize)) {
        return res.status(400).json({
            status: "Bad request",
            message: `Prize ID: ${body.prize} không hợp lệ`
        });
    }
    const prizeID = await prizeModel.findById(body.prize);
    if (!prizeID) {
        return res.status(400).json({
            status: "Bad request",
            message: `Prize ID: ${body.prize} không tồn tại`
        });
    }

    const newPrizeHistory = {
        _id: new mongoose.Types.ObjectId(),
        user: body.user,
        prize: body.prize
    };

    try {
        const dataPrizeHistory = await prizeHistoryModel.create(newPrizeHistory);
        return res.status(200).json({
            status: "Create new prize history successfully",
            data: dataPrizeHistory
        });
        //Nếu muốn trả về kết quả ở postman mà không có dòng:
        //     status: "Create new prize history successfully",
        //     "dataPrizeHistory": 
        //Thì sử dụng như dưới
        return res.status(200).json(dataPrizeHistory);
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        });
    }
};

const updatePrizeHistoryById = async (req, res) => {
    const body = req.body;
    const prizeHisID = req.params.prizeHisId;
    try {
        if (!mongoose.Types.ObjectId.isValid(prizeHisID)) {
            return res.status(400).json({
                status: "Bad request",
                message: `Prize History ID: ${prizeHisID} không hợp lệ`
            });
        }
        const dataPrizeHisID = await prizeHistoryModel.findById(prizeHisID);
        if (!dataPrizeHisID) {
            return res.status(400).json({
                status: "Bad request",
                message: `Prize History ID: ${prizeHisID} không tồn tại`
            });
        }
        if (body.user !== undefined) {
            if ((!mongoose.Types.ObjectId.isValid(body.user))) {
                return res.status(400).json({
                    status: "Bad request",
                    message: `User: ${body.user} không hợp lệ`
                });
            }

            const dataUserID = await userModel.findById(body.user);
            if ((!mongoose.Types.ObjectId.isValid(dataUserID))) {
                return res.status(400).json({
                    status: "Bad request",
                    message: `User: ${body.user} không hợp lệ..`
                });

            }
        }
        if (body.prize !== undefined) {
            if ((!mongoose.Types.ObjectId.isValid(body.prize))) {
                return res.status(400).json({
                    status: "Bad request",
                    message: `Prize: ${body.prize} không hợp lệ`
                });
            }
            const dataPrize = await prizeModel.findById(body.prize);
            if ((!mongoose.Types.ObjectId.isValid(dataPrize))) {
                return res.status(400).json({
                    status: "Bad request",
                    message: `Prize: ${body.prize} không hợp lệ...`
                });
            }
        }
        const updatePrize = {};
        updatePrize.user = body.user;
        updatePrize.prize = body.prize;

        const data = await prizeHistoryModel.findByIdAndUpdate(prizeHisID, updatePrize);
        return res.status(200).json({
            status: `Update prize history by ID: ${prizeHisID} successfully`,
            data: data
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        })
    }
};

const deletePrizeHistoryById = async (req, res) => {
    const prizeHisID = req.params.prizeHistoryId;
    if (!mongoose.Types.ObjectId.isValid(prizeHisID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `Prize History ID: ${prizeHisID} không hợp lệ`
        });
    }
    const dataPrizeHisID = await prizeHistoryModel.findById(prizeHisID);
    if (!dataPrizeHisID) {
        return res.status(400).json({
            status: "Bad request",
            message: `Prize history ID: ${prizeHisID} không tồn tại`
        });
    }

    try {
        await prizeHistoryModel.findByIdAndDelete(prizeHisID);
        return res.status(200).json({
            status: `Delete prize history by ID: ${prizeHisID} successfully`
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        });
    }
};
module.exports = {
    getAllPrizeHistory,
    getPrizeHistoryById,
    createPrizeHistory,
    updatePrizeHistoryById,
    deletePrizeHistoryById
}