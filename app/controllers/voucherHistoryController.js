const mongoose = require("mongoose");
const voucherHistoryModel = require("../models/voucherHistoryModel");
const userModel = require("../models/userModel");
const voucherModel = require("../models/voucherModel");

const getAllVoucherHistory = async (req, res) => {
    try {
        //Kiểm tra nếu req query có trường user thì bỏ vào condition
        //nếu muốn lấy riêng của user đó thì http://localhost:8000/api/voucherhistory?user=xxxxx
        let condition = {};
        if(req.query.user){
            condition.user = req.query.user;
        }
        //tạo phân trang
        const currentPage = parseInt(req.query.page) || 1;
        const perPage = parseInt(req.query.limit) || 10;
        const skip = (currentPage - 1) * perPage;
        const limit = perPage
        const dataVouHis = await voucherHistoryModel.find(condition).skip(skip).limit(limit)
            .populate({ path: "user", select: "username" })
            .populate({ path: "voucher", select: "code" })
            .exec();
        return res.status(200).json({
            status: "Get all voucher history successfully",
            data: dataVouHis
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        });
    }
};

const getVoucherHistoryById = async (req, res) => {
    const voucherHistoryID = req.params.voucherHistoryId;

    if (!mongoose.Types.ObjectId.isValid(voucherHistoryID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `Voucher history ID: ${voucherHistoryID} không hợp lệ`
        });
    }

    try {
        const dataVouHisID = await voucherHistoryModel.findById(voucherHistoryID)
            .populate({ path: "user", select: "username" })
            .populate({ path: "voucher", select: "code" })
            .exec();
        if (!dataVouHisID) {
            return res.status(400).json({
                status: "Bad request",
                message: `Voucher history ID: ${voucherHistoryID} không tồn tại trong hệ thống`
            });
        }
        return res.status(200).json({
            status: `Get voucher history by ID: ${voucherHistoryID} successfully`,
            data: dataVouHisID
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        });
    }
};

const createVoucherHistory = async (req, res) => {
    const body = req.body;

    if (!mongoose.Types.ObjectId.isValid(body.user)) {
        return res.status(400).json({
            status: "Bad request",
            message: `User ID: ${body.user} không hợp lệ`
        });
    }

    const userID = await userModel.findById(body.user);
    if (!userID) {
        return res.status(400).json({
            status: "Bad request",
            message: `User ID: ${body.user} không tồn tại`
        });
    }

    if (!mongoose.Types.ObjectId.isValid(body.voucher)) {
        return res.status(400).json({
            status: "Bad request",
            message: `Voucher ID: ${body.voucher} không hợp lệ`
        });
    }

    const voucherID = await voucherModel.findById(body.voucher);
    if (!voucherID) {
        return res.status(400).json({
            status: "Bad request",
            message: `Voucher ID: ${body.voucher} không tồn tại`
        });
    }

    const newVoucherHistory = {
        _id: new mongoose.Types.ObjectId(),
        user: body.user,
        voucher: body.voucher
    };

    try {
        const dataVouHis = await voucherHistoryModel.create(newVoucherHistory);
        return res.status(201).json({
            status: "Create new voucher history successfully",
            data: dataVouHis
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        });
    }
};

const updateVoucherHistoryById = async (req, res) => {
    try {
        const voucherHistoryID = req.params.voucherHistoryId;
        const body = req.body;

        if (!mongoose.Types.ObjectId.isValid(voucherHistoryID)) {
            return res.status(400).json({
                status: "Bad request",
                message: `Voucher history ID: ${voucherHistoryID} không hợp lệ`
            });
        }
        const dataVouHis = await voucherHistoryModel.findById(voucherHistoryID);
        if (!dataVouHis) {
            return res.status(400).json({
                status: "Bad request",
                message: `Voucher history ID: ${voucherHistoryID} không tồn tại trong hệ thống`
            });
        }
        if (body.user !== undefined) {
            if (!mongoose.Types.ObjectId.isValid(body.user)) {
                return res.status(400).json({
                    status: "Bad request",
                    message: `User ID: ${body.user} không hợp lệ`
                });
            }
            const dataUser = await userModel.findById(body.user);
            if (!dataUser) {
                return res.status(400).json({
                    status: "Bad request",
                    message: `USer ID ${body.user} không tồn tại trong hệ thống`
                });
            }
        }
        if (body.voucher !== undefined) {
            if (!mongoose.Types.ObjectId.isValid(body.voucher)) {
                return res.status(400).json({
                    status: "Bad request",
                    message: `Voucher ID: ${body.voucher} không hợp lệ`
                });
            }
            const dataVoucher = await voucherModel.findById(body.voucher);
            if (!dataVoucher) {
                return res.status(400).json({
                    status: "Bad request",
                    message: `Voucher ID: ${body.voucher} không tồn tại trong hệ thống`
                });
            }
        }

        const updateVoucherHistory = {};
        updateVoucherHistory.user = body.user;
        updateVoucherHistory.voucher = body.voucher;

        const data = await voucherHistoryModel.findByIdAndUpdate(voucherHistoryID, updateVoucherHistory);
        return res.status(200).json({
            status: `Update voucher history by ID: ${voucherHistoryID} successfully`,
            data: data
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        });
    }

};

const deleteVoucherHistoryById = async (req, res) => {
    const voucherHistoryID = req.params.voucherHistoryId;

    if (!mongoose.Types.ObjectId.isValid(voucherHistoryID)) {
        return res.status(400).json({
            status: "Bad request",
            message: `Voucher history ID: ${voucherHistoryID} không hợp lệ`
        });
    }
    const dataVouHisID = await voucherHistoryModel.findById(voucherHistoryID);
    if (!dataVouHisID) {
        return res.status(400).json({
            status: "Bad request",
            message: `Voucher history ID: ${voucherHistoryID} không tồn tại`
        });
    }
    try {
        await voucherHistoryModel.findByIdAndDelete(voucherHistoryID);
        return res.status(200).json({
            status: `Delete voucher history ID: ${voucherHistoryID} successfully`
        });
    } catch (error) {
        return res.status(500).json({
            status: "Internal server error",
            message: error.message
        });
    }
};

module.exports = {
    getAllVoucherHistory,
    getVoucherHistoryById,
    createVoucherHistory,
    updateVoucherHistoryById,
    deleteVoucherHistoryById
}