const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const SchemaPrizeHistory = new Schema({
    user: {
        type: mongoose.Types.ObjectId,
        required: true,
        ref: "User"
    },
    prize: {
        type: mongoose.Types.ObjectId,
        required: true,
        ref: "Prize"
    }
},{
    timestamps: true
});

module.exports = mongoose.model("PrizeHistory", SchemaPrizeHistory);