const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const diceHistorySchema = new Schema({
    user: {
        type: mongoose.Types.ObjectId,
        required: true,
        ref: "User"
    },
    dice: {
        type: Number,
        required: true
    }
}, {
    timestamps: true
});

module.exports = mongoose.model("DiceHistory", diceHistorySchema);