const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const voucherSchema = new Schema({
    code: {
        type: String,
        required: true,
        unique: true
    },
    discount: {
        type: Number,
        required: true
    },
    note: {
        type: String,
        required: false
    }
}, {
    timestamps: true
});

module.exports = mongoose.model("Voucher", voucherSchema);