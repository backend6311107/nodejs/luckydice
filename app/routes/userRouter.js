const express = require("express");
const router = express.Router();
const userMiddleware = require("../middlewares/userMiddleware");
const userController = require("../controllers/userController");

router.get("/users", userMiddleware.getAllUserMiddleware, userController.getAllUser);

router.get("/users/:userId", userMiddleware.getUserByIdMiddleware, userController.getUserById);

router.post("/users", userMiddleware.createUserMiddleware, userController.createUser);

router.put("/users/:userId", userMiddleware.updateUserByIdMiddleware, userController.updateUserById);

router.delete("/users/:userId", userMiddleware.deleteUserByIdMiddleware, userController.deleteUserById);

module.exports = router;