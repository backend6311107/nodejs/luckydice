const express = require("express");
const router = express.Router();
const diceHistoryMiddleware = require("../middlewares/diceHistoryMiddleware");
const diceHistoryController = require("../controllers/diceHistoryController");

router.get("/dicehistory", diceHistoryMiddleware.getAllDiceHistoryMiddleware, diceHistoryController.getAllDiceHistory);

router.get("/dicehistory/:diceHistoryId", diceHistoryMiddleware.getDiceHistoryByIdMiddleware, diceHistoryController.getDiceHistoryById);

router.post("/dicehistory", diceHistoryMiddleware.createDiceHistoryMiddleware, diceHistoryController.createDiceHistory);

router.put("/dicehistory/:diceHistoryId", diceHistoryMiddleware.updateDiceHistoryMiddleware, diceHistoryController.updateDiceHistoryById);

router.delete("/dicehistory/:diceHistoryId", diceHistoryMiddleware.deleteDiceHistoryByIdMiddleware, diceHistoryController.deleteDiceHistoryById);

module.exports = router;