const express = require("express");
const router = express.Router();
const voucherMiddleware = require("../middlewares/voucherMiddleware");
const voucherController = require("../controllers/voucherController");

router.get("/vouchers", voucherMiddleware.getAllVoucherMiddleware, voucherController.getAllVoucher);

router.get("/vouchers/:voucherId", voucherMiddleware.getVoucherByIdMiddleware, voucherController.getVoucherById);

router.post("/vouchers", voucherMiddleware.createVoucherMiddleware, voucherController.createVoucher);

router.put("/vouchers/:voucherId", voucherMiddleware.updateVoucherByIdMiddleware, voucherController.updateVoucherById);

router.delete("/vouchers/:voucherId", voucherMiddleware.deleteVoucherByIdMiddleware, voucherController.deleteVoucherById);

module.exports = router;