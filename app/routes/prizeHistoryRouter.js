const express = require("express");
const router = express.Router();
const priHiMiddleware = require("../middlewares/prizeHistoryMiddleware");
const priHiController = require("../controllers/prizeHistoryController");

router.get("/prizehistory", priHiMiddleware.getAllPrizeHistoryMiddleware, priHiController.getAllPrizeHistory);

router.get("/prizehistory/:prizeHistoryId", priHiMiddleware.getPrizeHistoryByIdMiddleware, priHiController.getPrizeHistoryById);

router.post("/prizehistory", priHiMiddleware.createPrizeHistoryMiddleware, priHiController.createPrizeHistory);

router.put("/prizehistory/:prizeHisId", priHiMiddleware.updatePrizeHistoryMiddleware, priHiController.updatePrizeHistoryById)

router.delete("/prizehistory/:prizeHistoryId", priHiMiddleware.deletePrizeHistoryByIdMiddleware, priHiController.deletePrizeHistoryById);

module.exports = router;