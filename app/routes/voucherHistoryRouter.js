const express = require("express");
const router = express.Router();
const voucherHistoryMiddleware = require("../middlewares/voucherHistoryMiddleware");
const voucherHistoryController = require("../controllers/voucherHistoryController");

router.get("/voucherhistory", voucherHistoryMiddleware.getAllVoucherHistoryMiddleware, voucherHistoryController.getAllVoucherHistory);

router.get("/voucherhistory/:voucherHistoryId", voucherHistoryMiddleware.getVoucherHistoryByIdMiddleware, voucherHistoryController.getVoucherHistoryById);

router.post("/voucherhistory", voucherHistoryMiddleware.createVoucherHistoryMiddleware, voucherHistoryController.createVoucherHistory);

router.put("/voucherhistory/:voucherHistoryId", voucherHistoryMiddleware.updateVoucherHistoryByIdMiddleware, voucherHistoryController.updateVoucherHistoryById);

router.delete("/voucherhistory/:voucherHistoryId", voucherHistoryMiddleware.deleteVoucherHistoryByIdMiddleware, voucherHistoryController.deleteVoucherHistoryById);

module.exports = router;