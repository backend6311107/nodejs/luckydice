const express = require("express");
const router = express.Router();
const prizeMiddleware = require("../middlewares/prizeMiddleware");
const prizeController = require("../controllers/prizeController");

router.get("/prizes", prizeMiddleware.getAllPrizeMiddleware, prizeController.getAllPrize);

router.get("/prizes/:prizeId", prizeMiddleware.getPrizeByIdMiddleware, prizeController.getPrizeById);

router.post("/prizes", prizeMiddleware.createPrizeMiddleware, prizeController.createPrize);

router.put("/prizes/:prizeId", prizeMiddleware.updatePrizeByIdMiddleware, prizeController.updatePrizeById);

router.delete("/prizes/:prizeId", prizeMiddleware.deletePrizeByIdMiddleware, prizeController.deletePrizeById);

module.exports = router;