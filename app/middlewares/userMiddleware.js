const getAllUserMiddleware = (req, res, next) => {
    console.log("Get all user middleware");
    next();
}

const getUserByIdMiddleware = (req, res, next) => {
    console.log("Get all user by ID middleware");
    next();
}

const createUserMiddleware = (req, res, next) => {
    console.log("Create new user middleware");
    next();
};

const updateUserByIdMiddleware = (req, res, next) => {
    console.log("Update user by ID middleware");
    next();
};

const deleteUserByIdMiddleware = (req, res, next) => {
    console.log(`Delete user by ID middleware`);
    next();
};

module.exports = {
    getAllUserMiddleware,
    getUserByIdMiddleware,
    createUserMiddleware,
    updateUserByIdMiddleware,
    deleteUserByIdMiddleware
};