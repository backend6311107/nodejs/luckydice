const getAllPrizeHistoryMiddleware = (req, res, next) => {
    console.log("Get all prize history middleware");
    next();
};

const getPrizeHistoryByIdMiddleware = (req, res, next) => {
    console.log("get prize history by ID middleware");
    next();
};

const createPrizeHistoryMiddleware = (req, res, next) => {
    console.log("Create prize history middleware");
    next();
};

const updatePrizeHistoryMiddleware = (req, res, next) => {
    console.log("Update prize history by ID middleware");
    next();
}

const deletePrizeHistoryByIdMiddleware = (req, res, next) => {
    console.log("Delete prize history by ID middleware");
    next();
}

module.exports = {
    getAllPrizeHistoryMiddleware,
    getPrizeHistoryByIdMiddleware,
    createPrizeHistoryMiddleware,
    updatePrizeHistoryMiddleware,
    deletePrizeHistoryByIdMiddleware
}