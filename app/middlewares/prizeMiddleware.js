const getAllPrizeMiddleware = (req, res, next) => {
    console.log("Get all prize middleware");
    next();
};

const getPrizeByIdMiddleware = (req, res, next) => {
    console.log("Get prize by ID middleware");
    next();
};

const createPrizeMiddleware = (req, res, next) => {
    console.log("Create new middleware");
    next();
};

const updatePrizeByIdMiddleware = (req, res, next) => {
    console.log("Update prize by ID middleware");
    next();
};

const deletePrizeByIdMiddleware = (req, res, next) =>{
    console.log("Delete prize by ID middleware");
    next();
};

module.exports = {
    getAllPrizeMiddleware,
    getPrizeByIdMiddleware,
    createPrizeMiddleware,
    updatePrizeByIdMiddleware,
    deletePrizeByIdMiddleware
}