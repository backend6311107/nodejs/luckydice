const getAllDiceHistoryMiddleware = (req, res, next) => {
    console.log("Get all dice history middleware");
    next();
};

const getDiceHistoryByIdMiddleware = (req, res, next) => {
    console.log("Get dice history by ID middleware");
    next();
};

const createDiceHistoryMiddleware = (req, res, next) => {
    console.log("Create new dice history middleware");
    next();
};

const updateDiceHistoryMiddleware = (req, res, next) => {
    console.log("Update dice history by ID middleware");
    next();
};

const deleteDiceHistoryByIdMiddleware = (req, res, next) => {
    console.log("Delete dice history by ID middleware");
    next();
};

module.exports = {
    getAllDiceHistoryMiddleware,
    getDiceHistoryByIdMiddleware,
    createDiceHistoryMiddleware,
    updateDiceHistoryMiddleware,
    deleteDiceHistoryByIdMiddleware
}