const getAllVoucherHistoryMiddleware = (req, res, next) => {
    console.log("Get all voucher history middleware");
    next();
};

const getVoucherHistoryByIdMiddleware = (req, res, next) => {
    console.log("Get voucher history by ID middleware");
    next();
};

const createVoucherHistoryMiddleware = (req, res, next) => {
    console.log("Create new voucher history middleware");
    next();
};

const updateVoucherHistoryByIdMiddleware = (req, res, next) => {
    console.log("Update voucher history by ID middleware");
    next();
};

const deleteVoucherHistoryByIdMiddleware = (req, res, next) =>{
    console.log("Update voucher history by ID middleware");
    next();
};

module.exports = {
    getAllVoucherHistoryMiddleware,
    getVoucherHistoryByIdMiddleware,
    createVoucherHistoryMiddleware,
    updateVoucherHistoryByIdMiddleware,
    deleteVoucherHistoryByIdMiddleware
}